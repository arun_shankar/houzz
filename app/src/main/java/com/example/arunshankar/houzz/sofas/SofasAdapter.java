package com.example.arunshankar.houzz.sofas;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudinary.Transformation;
import com.example.arunshankar.houzz.utils.InspirationsDetails;
import com.example.arunshankar.houzz.MainApplication;
import com.example.arunshankar.houzz.utils.PixelUtil;
import com.example.arunshankar.houzz.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by arunshankar on 08/04/15.
 */
public class SofasAdapter extends RecyclerView.Adapter<SofasAdapter.InspirationsViewHolder> {

    private List<InspirationsDetails> inspirationsDetailsList;
    Context context;
    DisplayImageOptions optionsBrowse;

    public SofasAdapter(List<InspirationsDetails> inspirationsDetailsList, Context context) {
        this.inspirationsDetailsList = inspirationsDetailsList;
        this.context = context;
        optionsBrowse = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.white_bg)
                .showImageForEmptyUri(R.drawable.white_bg)
                .showImageOnFail(R.drawable.white_bg)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
    }


    @Override
    public int getItemCount() {
        return inspirationsDetailsList.size();
    }

    @Override
    public void onBindViewHolder(final InspirationsViewHolder InspirationsViewHolder, int i) {
        final InspirationsDetails ci = inspirationsDetailsList.get(i);
        InspirationsViewHolder.vName.setText(ci.getType());
        InspirationsViewHolder.vSurname.setText(ci.getColor());
        InspirationsViewHolder.vEmail.setText(ci.getUpholstery());
        InspirationsViewHolder.vTitle.setText(ci.getTitle());
        InspirationsViewHolder.vName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //remove(ci);
            }
        });
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay()
                .getMetrics(dm);

        int width = dm.widthPixels - PixelUtil.dpToPx(context, 16);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            InspirationsViewHolder.showCasePic.setPadding(0,PixelUtil.dpToPx(context,1),PixelUtil.dpToPx(context,7),0);
        }
        InspirationsViewHolder.showCasePic.setLayoutParams(new FrameLayout.LayoutParams(width, FrameLayout.LayoutParams.WRAP_CONTENT));

        String url = MainApplication.getCloudinary().url().transformation(
                new Transformation().width(width)).signed(true).
                generate(ci.getImageUrl() + ".jpg");
        ImageLoader.getInstance().displayImage(url, InspirationsViewHolder.showCasePic, optionsBrowse,null);
        width = dm.widthPixels;
        url = MainApplication.getCloudinary().url().transformation(
                new Transformation().width(width)).signed(true).
                generate(ci.getImageUrl() + ".jpg");
        ImageLoader.getInstance().displayImage(url, new ImageView(context), optionsBrowse,null);

        InspirationsViewHolder.showCasePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] screenLocation = new int[2];
                v.getLocationOnScreen(screenLocation);
                /*Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("inspirations", ci)
                        .putExtra("left", screenLocation[0])
                        .putExtra("top", screenLocation[1])
                        .putExtra("width", v.getWidth())
                        .putExtra("height", v.getHeight())
                        .putExtra(
                                "resource",
                                ci.getImageUrl())
                        .putExtra("product",
                                ci.getImageUrl())

                        .putExtra("positionInList",0)
                        .putExtra("position", 0);

                ((Activity) context).startActivity(intent);
                ((Activity) context).overridePendingTransition(0, 0);*/

            }
        });

        InspirationsViewHolder.card_view.setRadius(PixelUtil.dpToPx(context,4));

    }

    @Override
    public InspirationsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_layout, viewGroup, false);

        return new InspirationsViewHolder(itemView);
    }

    public static class InspirationsViewHolder extends RecyclerView.ViewHolder {

        protected TextView vName;
        protected TextView vSurname;
        protected TextView vEmail;
        protected TextView vTitle;
        protected ImageView showCasePic;
        protected CardView card_view;
        protected TextView favorite;

        public InspirationsViewHolder(View v) {
            super(v);
            vName =  (TextView) v.findViewById(R.id.txtName);
            vSurname = (TextView)  v.findViewById(R.id.txtSurname);
            vEmail = (TextView)  v.findViewById(R.id.txtEmail);
            vTitle = (TextView) v.findViewById(R.id.title);
            showCasePic = (ImageView) v.findViewById(R.id.showCasePic);
            card_view = (CardView) v.findViewById(R.id.card_view);
            favorite = (TextView) v.findViewById(R.id.favorite);
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}
