package com.example.arunshankar.houzz.sofas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.arunshankar.houzz.utils.InspirationsDetails;
import com.example.arunshankar.houzz.utils.InspirationsNetworkUtils;
import com.example.arunshankar.houzz.R;
import com.example.arunshankar.houzz.utils.SwipeableRecyclerViewTouchListener;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by arunshankar on 15/04/15.
 */
public class SofasBrowseFragment extends Fragment implements InspirationsNetworkUtils.ParseQueryListener {

    ArrayList<InspirationsDetails> inspirationsDetailsArrayList = new ArrayList<InspirationsDetails>();
    RecyclerView sofasList;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_browse_sofas,null);
        InspirationsNetworkUtils.getAllCars(0, 50, this, new Date(0));
        sofasList = (RecyclerView) rootView.findViewById(R.id.sofasList);
        sofasList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        sofasList.setLayoutManager(llm);
        sofasList.setAdapter(new SofasAdapter(inspirationsDetailsArrayList, getActivity()));

        SwipeableRecyclerViewTouchListener swipeTouchListener =
                new SwipeableRecyclerViewTouchListener(sofasList,
                        new SwipeableRecyclerViewTouchListener.SwipeListener() {

                            @Override
                            public boolean canSwipeLeft(int position) {
                                return true;
                            }

                            @Override
                            public boolean canSwipeRight(int position) {
                                return false;
                            }

                            @Override
                            public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    inspirationsDetailsArrayList.remove(position);
                                    sofasList.getAdapter().notifyItemRemoved(position);
                                }
                                sofasList.getAdapter().notifyDataSetChanged();
                            }

                            @Override
                            public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
                                for (int position : reverseSortedPositions) {
                                    inspirationsDetailsArrayList.remove(position);
                                    sofasList.getAdapter().notifyItemRemoved(position);
                                }
                                sofasList.getAdapter().notifyDataSetChanged();
                            }
                        });
        sofasList.addOnItemTouchListener(swipeTouchListener);

        return rootView;
    }

    @Override
    public void recordFetched(InspirationsDetails inspirationsDetails) {
        inspirationsDetailsArrayList.add(inspirationsDetails);
        sofasList.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void allRecordsFetched() {

    }
}
