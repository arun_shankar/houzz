package com.example.arunshankar.houzz.inspirations;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.arunshankar.houzz.utils.InspirationsDetails;
import com.example.arunshankar.houzz.R;

/**
 * Created by arunshankar on 15/04/15.
 */
public class InspirationsDetailFragment extends Fragment {

    TextView category;
    TextView color;
    TextView description;
    TextView designStyle;
    TextView numberOfSeats;
    TextView title;
    TextView type;
    TextView upholstery;

    ScrollView scrollView;
    private boolean hasReachedTop = false;
    private boolean onLaunch = true;
    private LinearLayout linearLayout;
    private float y1,y2;
    static final int MIN_DISTANCE = 150;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail_inspirations,null);

        Bundle bundle = getArguments();
        InspirationsDetails inspirationsDetails = (InspirationsDetails) bundle.getSerializable("inspirations");

        scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);
        linearLayout = (LinearLayout)rootView.findViewById(R.id.linearLayout);

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {

                onLaunch=false;

                if(scrollView.getScrollY()==0)
                    hasReachedTop=true;
                else
                    hasReachedTop=false;

            }
        });

        category = (TextView) rootView.findViewById(R.id.category);
        color = (TextView) rootView.findViewById(R.id.color);
        description = (TextView) rootView.findViewById(R.id.description);
        designStyle = (TextView) rootView.findViewById(R.id.designStyle);
        numberOfSeats = (TextView) rootView.findViewById(R.id.numberOfSeats);
        title = (TextView) rootView.findViewById(R.id.title);
        type = (TextView) rootView.findViewById(R.id.type);
        upholstery = (TextView) rootView.findViewById(R.id.upholstery);

        category.setText(inspirationsDetails.getCategory());
        color.setText(inspirationsDetails.getColor());
        description.setText(inspirationsDetails.getDescription());
        designStyle.setText(inspirationsDetails.getDesignStyle());
        numberOfSeats.setText("" + inspirationsDetails.getNumberOfSeats());
        title.setText(inspirationsDetails.getTitle());
        type.setText(inspirationsDetails.getType());
        upholstery.setText(inspirationsDetails.getUpholstery());

        linearLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if(onLaunch)
                    hasReachedTop=true;
                if (hasReachedTop){
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            y1 = event.getY();
                            scrollView.requestDisallowInterceptTouchEvent(true);
                            break;
                        case MotionEvent.ACTION_UP:
                            y2 = event.getY();
                            float deltaY = y2 - y1;
                            if ((deltaY) > MIN_DISTANCE) {
                                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                    ft.setCustomAnimations(0, R.anim.abc_slide_out_bottom);
                                    ft.remove(InspirationsDetailFragment.this);
                                    ft.commit();
                            } else {
                                // consider as something else - a screen tap for example
                                onLaunch=false;
                                hasReachedTop=false;
                                scrollView.requestDisallowInterceptTouchEvent(false);
                            }
                            scrollView.requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                return true;
            }else {
                    scrollView.requestDisallowInterceptTouchEvent(false);
                    return false;
                }
        }
    });

        return rootView;
    }
}
