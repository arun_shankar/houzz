package com.example.arunshankar.houzz;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.cloudinary.Cloudinary;
import com.example.arunshankar.houzz.utils.ImageLruCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.parse.Parse;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by arunshankar on 12/11/2014.
 */
public class MainApplication extends Application {

    private static MainApplication sInstance;

    private static Cloudinary cloudinary;

    private ImageLruCache imageCache;
    private RequestQueue queue;
    private com.android.volley.toolbox.ImageLoader imageLoader;


    public static void initImageLoader(Context context) {
        // This configuration tuning is custom. You can tune every option, you may tune some of them,
        // or you can create default configuration by
        //  ImageLoaderConfiguration.createDefault(this);
        // method.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 50 Mb
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
    }

    public void onCreate() {
        Map config = new HashMap();
        sInstance = this;
        config.put(getResources().getString(R.string.cloudNameKey), getResources().getString(R.string.cloudName));
        config.put(getResources().getString(R.string.apiKey), getResources().getString(R.string.api));
        config.put(getResources().getString(R.string.apiSecretKey), getResources().getString(R.string.apiSecret));
        config.put("secure", true);
        cloudinary = new Cloudinary(config);
        initImageLoader(getApplicationContext());

        Parse.initialize(this, getResources().getString(R.string.applicationId), getResources().getString(R.string.clientKey));
        // configure Flurry
        imageCache = new ImageLruCache(ImageLruCache.getDefaultLruCacheSize());
        queue = Volley.newRequestQueue(this);
        imageLoader = new com.android.volley.toolbox.ImageLoader(queue, imageCache);

// init Flurry
        //Parse.initialize(this, "DAfVBeHyWakSmyuaXIH61LQUQACaEOXEINWbw753", "oujTLhII6sqkso9yS8q24joh5h0ZchaNqLXd3wO0");
    }

    public synchronized static MainApplication getInstance() {
        return sInstance;
    }

    public static Cloudinary getCloudinary() {
        return cloudinary;
    }

    public RequestQueue getRequestQueue() {
        return queue;
    }

    public ImageLruCache getCache() {
        return imageCache;
    }
    /**
     * Used to return the singleton imageloader
     * that utilizes the image lru cache.
     * @return ImageLoader
     */
    public com.android.volley.toolbox.ImageLoader getImageLoader(){
        return imageLoader;
    }

}
