package com.example.arunshankar.houzz.utils;

import java.io.Serializable;

/**
 * Created by arunshankar on 01/04/15.
 */
public class InspirationsDetails implements Serializable,Comparable<InspirationsDetails> {

    String objectId;
    String category;
    String color;
    String description;
    String designStyle;
    String imageUrl;
    int numberOfSeats;
    String title;
    String type;
    String upholstery;
    int rating;

    public InspirationsDetails(String objectId, String category, String color, String description, String designStyle,
                               String imageUrl, int numberOfSeats, String title, String type,
                               String upholstery, int rating) {
        this.objectId = objectId;
        this.category = category;
        this.color = color;
        this.description = description;
        this.designStyle = designStyle;
        this.imageUrl = imageUrl;
        this.numberOfSeats = numberOfSeats;
        this.title = title;
        this.type = type;
        this.upholstery = upholstery;
        this.rating = rating;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getCategory() {
        return category;
    }

    public String getColor() {
        return color;
    }

    public String getDescription() {
        return description;
    }

    public String getDesignStyle() {
        return designStyle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getUpholstery() {
        return upholstery;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public int compareTo(InspirationsDetails another) {
        return 0;
    }
}
