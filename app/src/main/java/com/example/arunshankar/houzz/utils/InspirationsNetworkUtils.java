package com.example.arunshankar.houzz.utils;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.arunshankar.houzz.MainApplication;
import com.example.arunshankar.houzz.R;
import com.parse.CountCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InspirationsNetworkUtils {

    public static int firstFetchCars = 50;
    public static int stepFetchCars = 50;
    static int carsFetched = 0;
    static int ctr = 0;
    static int totalCars = 0;
    static Date createdAt = new Date(0);

    public interface ParseQueryListener {
        public void recordFetched(InspirationsDetails inspirationsDetails);

        public void allRecordsFetched();
    }

    public static void fetchCarsData(ParseQueryListener listener) {
//        getAllCars(db, 0, firstFetchCars, listener);
        countAndFetchCars(listener, createdAt);
    }

    public static void countAndFetchCars(final ParseQueryListener listener, final Date createdAt) {
        final ParseQuery<ParseObject> itemParseQuery = new ParseQuery<ParseObject>("Item");
        itemParseQuery.whereNotEqualTo("sold", true);
        itemParseQuery.countInBackground(new CountCallback() {
            @Override
            public void done(int total, ParseException e) {
                totalCars = total;
                Log.e("count", totalCars + "");

            }
        });

    }

    public static Date getCreatedAt() {
        return createdAt;
    }

    public static void setCreatedAt(Date createdAt) {
        InspirationsNetworkUtils.createdAt = createdAt;
    }


    public static void getAllCars(int offset, int limit, final ParseQueryListener listener, final Date createdAt){
        String url = MainApplication.getInstance().getResources().getString(R.string.parse_base_url) +
                MainApplication.getInstance().getResources().getString(R.string.inspirations_class);
        List<NameValuePair> params = new LinkedList<NameValuePair>();
        params.add(new BasicNameValuePair("include", "category"));
        params.add(new BasicNameValuePair("order","-rating"));
        params.add(new BasicNameValuePair("limit", String.valueOf(limit)));
        params.add(new BasicNameValuePair("skip", String.valueOf(offset)));
        String paramString = URLEncodedUtils.format(params, "utf-8");
        url += paramString;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                final HashMap<String,String> objectIdHashMap = new HashMap<String, String>();
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    for(int i=0;i<jsonArray.length();i++) {
                        ctr++;
                        try{
                        final JSONObject itemObject = jsonArray.getJSONObject(i);
                        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                        String string1 = itemObject.getString("createdAt");
                        Date result1 = df1.parse(string1);
                        if (InspirationsNetworkUtils.createdAt.before(result1))
                            InspirationsNetworkUtils.createdAt = result1;
                            {
                                try {
                                    final JSONObject category = itemObject.getJSONObject("category");
                                    final String categoryType = category.optString("name");
                                    ArrayList<String> styles=new ArrayList<>();
                                    JSONObject locality=null;
                                    JSONObject location=null;
                                    InspirationsDetails inspirationsDetails =
                                            new InspirationsDetails(itemObject.optString("objectId"),
                                                    categoryType,itemObject.optString("color"),
                                                    itemObject.optString("description"),
                                                    itemObject.optString("designStyle"),
                                                    itemObject.optString("public_id"),
                                                    itemObject.optInt("seats"),
                                                    itemObject.optString("title"),
                                                    itemObject.optString("type"),
                                                    itemObject.optString("upholstrey"),
                                                    itemObject.optInt("rating"));
                                    listener.recordFetched(inspirationsDetails);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        if (ctr == stepFetchCars || (carsFetched == 0 && ctr == firstFetchCars)) {
                            ctr = 0;
                            carsFetched += firstFetchCars;
                            getAllCars(carsFetched, firstFetchCars, listener, createdAt);
                        } else if (ctr + carsFetched == totalCars) {
                            listener.allRecordsFetched();
                        }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }


            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Parse-Application-Id", "xcYoY8i8siwXFrycwZeGs9hGU4mbuUkVho9werop");
                params.put("X-Parse-REST-API-Key", "9ti4tRIdPtnHsEof8BHcdsJG4c7jxd9wL4f5S0BY");
                return params;
            }
        };
        MainApplication.getInstance().getRequestQueue().add(stringRequest);
    }

}
