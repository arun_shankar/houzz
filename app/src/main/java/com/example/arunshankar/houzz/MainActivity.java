package com.example.arunshankar.houzz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import com.example.arunshankar.houzz.inspirations.InspirationsActivity;
import com.example.arunshankar.houzz.sofas.SofasActivity;

/**
 * Created by arunshankar on 15/04/15.
 */
public class MainActivity extends ActionBarActivity {

    Button inspirationsButton,sofasButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inspirationsButton = (Button) findViewById(R.id.inspirationsButton);
        sofasButton = (Button) findViewById(R.id.sofasButton);

        inspirationsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,InspirationsActivity.class);
                startActivity(intent);
            }
        });

        sofasButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,SofasActivity.class);
                startActivity(intent);
            }
        });
    }
}
