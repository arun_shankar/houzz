package com.example.arunshankar.houzz.inspirations;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cloudinary.Transformation;
import com.example.arunshankar.houzz.utils.InspirationsDetails;
import com.example.arunshankar.houzz.utils.InspirationsNetworkUtils;
import com.example.arunshankar.houzz.MainApplication;
import com.example.arunshankar.houzz.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by arunshankar on 15/04/15.
 */
public class InspirationsBrowseFragment extends Fragment implements InspirationsNetworkUtils.ParseQueryListener {

    ArrayList<InspirationsDetails> inspirationsDetailsArrayList = new ArrayList<InspirationsDetails>();
    ViewPager inspirationsPager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_browse_inspirations,null);
        InspirationsNetworkUtils.getAllCars(0, 50, this, new Date(0));
        inspirationsPager = (ViewPager) rootView.findViewById(R.id.inspirationsPager);
        inspirationsPager.setOffscreenPageLimit(3);
        inspirationsPager.setPageMargin(0);
        inspirationsPager.setClipToPadding(true);
        inspirationsPager.setAdapter(new MyPagerAdapter(getActivity(),inspirationsDetailsArrayList));
        return rootView;
    }

    @Override
    public void recordFetched(InspirationsDetails inspirationsDetails) {
        inspirationsDetailsArrayList.add(inspirationsDetails);
        ((MyPagerAdapter) inspirationsPager.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void allRecordsFetched() {

    }

    private class MyPagerAdapter extends PagerAdapter {

        ArrayList<InspirationsDetails> inspirationsDetailsArrayList;
        Context context;

        private float y1,y2;
        static final int MIN_DISTANCE = -150;


        private MyPagerAdapter(Context context,ArrayList<InspirationsDetails> inspirationsDetailsArrayList) {
            this.inspirationsDetailsArrayList = inspirationsDetailsArrayList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return inspirationsDetailsArrayList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {

            DisplayMetrics dm = new DisplayMetrics();
            ((Activity)context).getWindowManager().getDefaultDisplay()
                    .getMetrics(dm);

            int width = dm.widthPixels;
            int heightOfImage = dm.heightPixels;

            DisplayImageOptions optionsBrowse = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.white_bg)
                    .showImageForEmptyUri(R.drawable.white_bg)
                    .showImageOnFail(R.drawable.white_bg)
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                    .build();

            final String url = MainApplication.getCloudinary().url().transformation(
                    new Transformation().width(width).height(heightOfImage).crop("fill")).signed(true).
                    generate(inspirationsDetailsArrayList.get(position).getImageUrl() + ".jpg");

            final LayoutInflater inflater = LayoutInflater.from(context);
            LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.pager_item, null);
            ImageView imageView = (ImageView) linearLayout.findViewById(R.id.imageView);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            ImageLoader.getInstance().displayImage(url, imageView, optionsBrowse, null);

            linearLayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:
                            y1 = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            y2 = event.getY();
                            float deltaY = y2-y1;
                            if ((deltaY) < MIN_DISTANCE)
                            {
                                FragmentTransaction ft = ((InspirationsActivity)context).getSupportFragmentManager().beginTransaction();
                                ft.setCustomAnimations(R.anim.abc_slide_in_bottom, 0);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("inspirations",inspirationsDetailsArrayList.get(position));
                                Fragment detailFragment = new InspirationsDetailFragment();
                                detailFragment.setArguments(bundle);
                                ft.add(R.id.container, detailFragment, "fragment").addToBackStack("");
                                ft.commit();
                            }
                            else
                            {
                                // consider as something else - a screen tap for example
                            }
                            break;
                    }
                    return true;
                }
            });
            container.addView(linearLayout);
            return linearLayout;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }

    }
}
